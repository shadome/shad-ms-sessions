# Motivation

This crate:
* provides an implementation of the `trait` `actix_session::storage::SessionStore` used in the gateway application for managing user session storage *(using the crate `sqlx` and the database `postgres`)*
* provides extended API functionalities, such as the possibility to clear the session of one particular user *(at least, the parts of their session related to their user account, such as the authentication token)*, useful for some operations like clearing a session when a website administrator or backoffice agent chooses to freeze or delete a user's account

# How is this crate served

***EDIT: unclear as of 23/01/2024***
This crate is not exposed as an `http` server. Instead, the compiled crate will be referenced by the application `shad-apps-gateway`. The URL will be different between local and production environments, requiring more configuration in `Cargo.toml`.

## Environment-specific relative paths of dependencies

***EDIT: unclear as of 23/01/2024***
When crate have local dependencies towards other crates *(which are not on a Internet-hosted repository such as crates.io)*, the paths may be different between each environment, and between each developer's local folder architecture.

The simplest way to address this issue is to make a linux symbolic link of the dependencies, for instance:

```sh
cd <current_workspace>
ln -s ~/<host_specific_path>/my_dependency
```

```toml
[dependencies]
my_dependency = { path = "my_dependency" }
```
