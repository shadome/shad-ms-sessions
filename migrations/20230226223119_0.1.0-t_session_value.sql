create table if not exists t_session_value (
    -- this PK is NOT published
    id uuid primary key,
    -- FK towards t_session (1-to-many)
    session_id uuid not null,
    -- left-hand side of the session record tuple
    key varchar not null,
    -- right-hand side of the session record tuple
    val varchar not null,
    -- session_id_fk
    constraint fk_session foreign key(session_id) references t_session(id));