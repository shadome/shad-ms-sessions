create table if not exists t_session (
    -- this PK is NOT published
    id uuid primary key,
    -- optional shad-apps-users.t_user.id virtual foreign key, if this is a logged in user's session
    user_id uuid,
    -- published "key" of a session, is NOT unique in this table, but associated key/val pairs should be
    token varchar not null,
    -- date after which the session is void
    -- a record may still exist after this time but it only awaits deletion and should never be read/updated again
    expires_at timestamp not null);