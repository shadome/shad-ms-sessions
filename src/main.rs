pub(crate) mod repositories;
pub(crate) use repositories::*;

pub(crate) mod routes;
pub(crate) use routes::*;

pub(crate) mod utils;
pub(crate) use utils::*;

    // TODO(shad): Careful when reading/updating sessions and session values which have expired
    // - expired sessions may still be trailing in the database! having a row matching a given session_key doesn't mean the row isn't outdated!
    // - when should the expired sessions be flushed? after each request? every X time intervals?


// Keep in sync with the content of the .env file
#[derive(Clone, Debug)]
pub struct Env {
    database_url: secrecy::SecretString,
    host: String,
    port: u16,
    secret_key: secrecy::SecretString,
}

fn init_env_variables() -> Env {
    dotenvy::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL env variable must be set").into();
    let host = std::env::var("HOST").expect("HOST env variable must be set");
    let port = std::env::var("PORT").expect("PORT env variable must be set").parse().expect("PORT env variable must be a i16");
    let secret_key = std::env::var("SECRET_KEY").expect("SECRET_KEY env variable must be set").into();
    Env { database_url, host, port, secret_key }
}

#[actix_web::main]
async fn main() -> Result<(), crate::AppError> {
    /* initialisation */
    // env variables
    let env = init_env_variables();
    // db connection pools
    let db_pool = sqlx::postgres::PgPool::connect(secrecy::ExposeSecret::expose_secret(&env.database_url)).await?;
    // repository layer
    let repos = repositories::Repositories::new(db_pool);
    /* application */
    routes::run_webapp(env, repos).await?;
    Ok(())
}
