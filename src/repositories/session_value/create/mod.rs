
impl super::SessionValue {

    pub(crate) fn new_creatable(
        session_id: uuid::Uuid,
        key: String,
        val: String,
    ) -> Result<SessionValueCreatable, anyhow::Error> {
        let session_value = super::SessionValue {
            id: uuid::Uuid::new_v4(),
            session_id,
            key,
            val,
        };
        
        let result = SessionValueCreatable { session_value };
        Ok(result)
    }

}

#[derive(Debug)]
pub(crate) struct SessionValueCreatable { session_value: super::SessionValue }

impl super::SessionValueRepository {

    pub(crate) async fn create(&self, session_value: &SessionValueCreatable) -> Result<(), anyhow::Error> {
        sqlx
            ::query!(
                r#"insert into t_session_value (id, session_id, key, val) values ($1, $2, $3, $4);"#,
                session_value.session_value.id,
                session_value.session_value.session_id,
                session_value.session_value.key,
                session_value.session_value.val)
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
