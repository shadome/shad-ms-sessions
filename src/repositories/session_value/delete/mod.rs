
impl super::SessionValueRepository {

    pub(crate) async fn delete(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        session_ids: Option<Vec<uuid::Uuid>>,
    ) -> Result<(), anyhow::Error> {
        sqlx
            ::query!(
                r#"
                    delete from t_session_value
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::uuid[] is null or session_id = any($2))
                ;"#,
                ids.as_deref(),
                session_ids.as_deref())
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
