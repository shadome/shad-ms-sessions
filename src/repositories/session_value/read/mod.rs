
impl super::SessionValueRepository {

    pub(crate) async fn read(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        session_ids: Option<Vec<uuid::Uuid>>,
        keys: Option<Vec<String>>,
    ) -> Result<Vec<crate::SessionValue>, anyhow::Error> {
        let result = sqlx
            ::query_as!(
                crate::SessionValue,
                r#"
                    select id, session_id, key, val
                    from t_session_value
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::uuid[] is null or session_id = any($2))
                        and ($3::varchar[] is null or key = any($3))
                ;"#,
                ids.as_deref(),
                session_ids.as_deref(),
                keys.as_deref())
            .fetch_all(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}
