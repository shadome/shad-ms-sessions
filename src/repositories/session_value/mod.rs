pub(super) mod create;
pub(super) mod read;
pub(super) mod delete;

#[derive(Clone, Debug)]
pub(crate) struct SessionValueRepository {
    pg_pool: std::sync::Arc<sqlx::postgres::PgPool>,
}

impl SessionValueRepository {
    
    pub(super) fn new(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self { pg_pool: pg_pool }
    }

}

/// A session is a list of session values
#[derive(Clone, Debug)]
pub(crate) struct SessionValue {
    // this PK is NOT published
    pub(crate) id: uuid::Uuid,
    pub(crate) session_id: uuid::Uuid,
    // key of the key-value pair of a session value
    pub(crate) key: String,
    // value of the key-value pair of a session value
    pub(crate) val: String,
}
