
impl super::SessionRepository {

    pub(crate) async fn update(
        &self,
        old_token: String,
        new_expires_at: chrono::NaiveDateTime,
    ) -> Result<(), anyhow::Error> {
        sqlx
            ::query!(
                r#"update t_session set expires_at = $1 where token = $2;"#,
                new_expires_at,
                old_token)
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
