
impl super::SessionRepository {

    pub(crate) async fn read(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        user_ids: Option<Vec<uuid::Uuid>>,
        tokens: Option<Vec<String>>,
    ) -> Result<Vec<crate::Session>, anyhow::Error> {
        let result = sqlx
            ::query_as!(
                crate::Session,
                r#"
                    select id, user_id, token, expires_at
                    from t_session
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::uuid[] is null or user_id = any($2))
                        and ($3::varchar[] is null or token = any($3))
                ;"#,
                ids.as_deref(),
                user_ids.as_deref(),
                tokens.as_deref())
            .fetch_all(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}
