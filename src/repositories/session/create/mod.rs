
impl super::Session {

    pub(crate) fn new_creatable(
        user_id: Option<uuid::Uuid>,
        expires_at: chrono::NaiveDateTime,
    ) -> Result<SessionCreatable, anyhow::Error> {
        let session = super::Session {
            id: uuid::Uuid::new_v4(),
            user_id,
            token: uuid::Uuid::new_v4().to_string(),
            expires_at,
        };
        
        let result = SessionCreatable { session };
        Ok(result)
    }

}

#[derive(Debug)]
pub(crate) struct SessionCreatable { session: super::Session }

impl super::SessionRepository {

    pub(crate) async fn create(&self, session: &SessionCreatable) -> Result<(), anyhow::Error> {
        sqlx
            ::query!(
                r#"insert into t_session (id, user_id, token, expires_at) values ($1, $2, $3, $4);"#,
                session.session.id,
                session.session.user_id,
                session.session.token,
                session.session.expires_at)
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
