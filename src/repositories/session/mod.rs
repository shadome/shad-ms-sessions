pub(super) mod create;
pub(super) mod read;
pub(super) mod update;
pub(super) mod delete;

#[derive(Clone, Debug)]
pub(crate) struct SessionRepository {
    pg_pool: std::sync::Arc<sqlx::postgres::PgPool>,
}

impl SessionRepository {
    
    pub(super) fn new(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self { pg_pool: pg_pool }
    }

}

#[derive(Clone, Debug)]
pub(crate) struct Session {
    // this PK is NOT published
    pub(crate) id: uuid::Uuid,
    // optional shad-apps-users.t_user.id virtual foreign key, if this is a logged in user's session
    pub(crate) user_id: Option<uuid::Uuid>,
    // published "key" of a session
    pub(crate) token: String,
    // datetime after which the session is void
    // a record may still exist after this time but it only awaits deletion and should never be read/updated again
    pub(crate) expires_at: chrono::NaiveDateTime,
}
