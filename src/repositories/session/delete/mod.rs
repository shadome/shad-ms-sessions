
impl super::SessionRepository {

    pub(crate) async fn delete(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        user_ids: Option<Vec<uuid::Uuid>>,
        tokens: Option<Vec<String>>,
    ) -> Result<(), anyhow::Error> {
        sqlx
            ::query!(
                r#"
                    delete from t_session
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::uuid[] is null or user_id = any($2))
                        and ($3::varchar[] is null or token = any($3))
                ;"#,
                ids.as_deref(),
                user_ids.as_deref(),
                tokens.as_deref())
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
