pub(super) mod session;
pub(super) use session::*;
pub(super) mod session_value;
pub(super) use session_value::*;

#[derive(Clone, Debug)]
pub(crate) struct Repositories {
   session_repo: SessionRepository,
   session_value_repo: SessionValueRepository,
}

impl Repositories {
   
   pub(crate) fn new(pg_pool: sqlx::postgres::PgPool) -> Self {
       let pg_pool = std::sync::Arc::new(pg_pool);
       Self {
           session_repo: SessionRepository::new(pg_pool.clone()),
           session_value_repo: SessionValueRepository::new(pg_pool.clone()),
       }
   }
   
   pub(crate) fn session_repo(&self) -> &SessionRepository { &self.session_repo }
   pub(crate) fn session_value_repo(&self) -> &SessionValueRepository { &self.session_value_repo }

   // pub(crate) fn get_session

}