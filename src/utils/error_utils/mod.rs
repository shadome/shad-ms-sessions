use std::error::Error;
use std::fmt;

/* AppError implementation, for simpler error management */

/// <p><b><u>! ELEMENTS IN THIS ENUM CAN NEVER BE MODIFIED OR DELETED !</u></b></p>
/// This enum, or prior versions of it, can be found in any service which uses this microservice as a dependency.
/// As a consequence, elements can only be <b><u>added</u></b> to this enum. Obsolete elements can be deprecated
/// but must keep existing and their integer value must never be re-used.
#[repr(u16)]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub(crate) enum ServerStatusCode {
    /* global codes */
    TechnicalError = 1,
}

impl std::fmt::Display for ServerStatusCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let val = *self as u16;
        write!(f, "{}", val)
    }
}

#[derive(Debug, serde::Serialize)]
pub(crate) struct AppError {
    /// information message to display in a blank error page
    pub(crate) message: String,
    /// http status code, e.g., 404 for NOT_FOUND, or 200 for OK
    #[serde(skip_serializing)]
    pub(crate) http_status_code: actix_web::http::StatusCode,
    /// exposed status code, similar to the http status code but managed by this microservice
    /// e.g., a code for the error "maximum number of users reached" or "user already exists" in case of a sign up
    /// this status code holds a functional value, i.e., it does not describe technical errors
    #[serde(serialize_with = "serialize_server_status_code")]
    pub(crate) server_status_code: ServerStatusCode,
}

impl AppError {
    pub(crate) fn new(message: String, server_status_code: ServerStatusCode) -> Self {
        Self {
            message,
            server_status_code,
            http_status_code:
                if server_status_code == ServerStatusCode::TechnicalError { 
                    actix_web::http::StatusCode::INTERNAL_SERVER_ERROR
                } else {
                    actix_web::http::StatusCode::BAD_REQUEST
                },
        }
    }
}

fn serialize_server_status_code<S>(x: &ServerStatusCode, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    s.serialize_u16(*x as u16)
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for AppError {}

impl From<AppError> for actix_web::HttpResponse {
    fn from(error: AppError) -> Self {
        let body = serde_json::to_string_pretty(&error).unwrap();
        actix_web::HttpResponseBuilder::new(error.http_status_code)
            .content_type(actix_web::http::header::ContentType::json())
            .body(body)
        }
}

/* From AppError to actix ResponseError, for enabling error propagation syntax ("await?", etc.) in handlers */

impl actix_web::error::ResponseError for AppError {
    fn error_response(&self) -> actix_web::HttpResponse {
        let body = serde_json::to_string_pretty(self).unwrap();
        actix_web::HttpResponseBuilder::new(self.http_status_code)
            .content_type(actix_web::http::header::ContentType::plaintext())
            .body(body)
            
    }
}

/* From everything to AppError, for simpler error management */

impl From<std::io::Error> for AppError {
    fn from(error: std::io::Error) -> Self {
        AppError {
            message: error.to_string(),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}

impl From<serde_json::Error> for AppError {
    fn from(error: serde_json::Error) -> Self {
        AppError {
            message: error.to_string(),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}
