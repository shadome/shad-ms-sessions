
pub(super) async fn run_webapp(
    env: crate::Env,
    repos: crate::Repositories,
) -> Result<(), crate::AppError> {
    let app_closure = move || {
        actix_web::App::new()
            // avoid generic http pages returned in case of internal panic
            .wrap(actix_web::middleware::ErrorHandlers::new())
            // enables logging in the console in which the server runs through the actix middleware
            .wrap(actix_web::middleware::Logger::default())
            // routes
            .route("/check_and_normalise/{email}", actix_web::web::get().to(check_and_normalise::route))
    };

    // the following should never stop looping
    actix_web::HttpServer::new(app_closure)
        .bind((env.host.as_str(), env.port))?
        .run()
        .await?;
    Ok(())
}
